//
//  AutoshrinkHeightUILabel.swift
//  TestUILabel
//
//  Created by Colin Reisterer on 1/3/15.
//  Copyright (c) 2015 akcode. All rights reserved.
//

import UIKit

class AutoshrinkHeightUILabel: UILabel {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // if label text height is greater than label frame height, shrink it to fit (autoshrinking only works for width)

        if let labelText = self.text {
            let fontSize = font.pointSize
            
            for var i = font.pointSize; i > 0; i-- {
                
                let newFont = font.fontWithSize(i)
                let attributes: NSDictionary = [NSFontAttributeName: newFont]
                let textRect: CGRect = labelText.boundingRectWithSize(self.frame.size, options: (NSStringDrawingOptions.UsesFontLeading), attributes: attributes, context: nil)
                if textRect.height <= self.frame.height {
                    font = newFont
                    break
                }
                
            }
        }
    }
    
}
