AutoshrinkHeightUILabel

A simple subclass of UILabel that will autoshrink text that will not fit in the UILabel's frame height. UILabel's default autoshrink behavior only shrinks text that does not fit in the frame width.
